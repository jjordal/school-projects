#include <stdio.h>
#include <string.h>
#include <iostream>
#include <map>
#include <vector>
#include <fstream>
using namespace std;



void fill_dictionary(map<string, int>& dict){
	string buffer;
	int index;
	ifstream dictfile ("diction10k.txt");
	if(dictfile.is_open()){
		while(dictfile.good()){
			getline(dictfile, buffer);
			index = buffer.find("\r");
			if(index != -1){
				buffer.erase(index, 1);
			}
			if(dict[buffer] == 0){
				dict[buffer] = 1;
			}
		}
		dictfile.close();
	}
}


bool split(string input, map<string, int> dict, map<int, int>& out_coor){
	string sub = "";
	int len = input.size();
	int split[len + 1] = { 0 };
	split[len] = 1;
	for(int i = len; i >= 0; i--){
		sub = "";
		for(int j = i; j < len; j++){
			sub += input[j];
			if(dict[sub] && split[j + 1]){
				split[i] = 1;
				out_coor[i] = (j + 1);
				break;
			}
		}
	}
	if(split[0] == 1){
		return true;
	}
	return false;
}

bool split_r(string input, map<string, int> dict, int index, int split[]){
	if(split[index]){
		return true;
	}
	if(index >= input.size()){
		return true;
	}
	for(int i = index; i < input.size(); i++){
		if(dict[input.substr(index, (i + 1) - index)] && split_r(input, dict, i + 1, split)){
			split[index] = 1;
			return split[index];
		}
	}
	return false;

}	

string split_str(map<int, int> indices, string in_str){
	string ret = in_str;
	string first;
	string last;
	int ctr = 0;
	int stop = in_str.size();
	int index = indices[0];
	while(index != stop){
		first = ret.substr(0, index + ctr);
		last = ret.substr(index + ctr, stop-index);
		ret = first + " " + last;
		ctr++;
		index = indices[index];
	}
	return ret;
}

string split_string_r(string input, int split[]){
	int len = input.size();
	int ctr = 0;
	string first;
	string last;
	string ret = input;
	for(int i = 1; i < len; i++){
		if(split[i] == 1){
			first = ret.substr(0, i + ctr);
			last = ret.substr(i + ctr, len - i);
			ret = first + " " + last;
			ctr++;	
		}
	}
	return ret;
}

int main(){

map<string, int> dict;
map<int, int> word_coor;
fill_dictionary(dict);
int lines;
cin >> lines;
string word;
string split_word;
for(int i = 0; i < lines; i++){
	cin >> word;
	cout << "Phrase number: " << i + 1 << endl;
	cout << word << endl;
	cout << endl;
	cout << "Iterative attempt:" << endl;
	if(split(word, dict, word_coor) == 1){
		cout << "YES, can be split" << endl;
		split_word = split_str(word_coor, word);
		cout << split_word << endl;		
	}else{
		cout << "NO, cannot be split" << endl;
	}
	cout << endl;
	cout << "memoized attempt: " << endl;
	int split_array[word.size()] = { 0 };
	int index = 0;
	if(split_r(word, dict, index, split_array) == 1){
		cout << "YES, can be split" << endl;
		split_word = split_string_r(word, split_array);
		cout << split_word << endl;
	}else{
		cout << "NO, cannot be split" << endl;
	}
	cout << endl;
}


}
