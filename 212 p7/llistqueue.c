/*
 * implementation for linked-list-based generic FIFO queue
 */

#include "ADTs/llistqueue.h"
/* any other includes needed for the implementation */
#include <stddef.h>
#include <stdlib.h>

typedef struct node{
    struct node *next;
    struct Data *data;
} Node;

typedef struct q_data {
    /* flesh out the instance specific data structure */
    Node *head;
    Node *tail;
    long count;
    void (*freeValue)(void *e);
} QData;
/* any other data structures needed */

typedef struct data{
    int Length;
    int Distance;
    int Num;
} Data;


 Node *createNode(struct Data *d){
    Node *p = (Node *)malloc(sizeof(Node));
    if(p != NULL){
        p->data = d;
        p->next = NULL;
    }
    return p;
}

static void q_destroy(const Queue *q) {
    /* implementation of the destroy() method */
    QData *qd = (QData *)q->self;
    free(qd);
    free((void *)q);
}

static void q_clear(const Queue *q) {
    /* implementation of the clear() method */
    QData *qd = (QData *)q->self;
    while(q->isEmpty(q) == 0){
            Data *d;
            q->dequeue(q, (void **)&d);
        }
    qd->head = NULL;
    qd->tail = NULL;
}

static int q_enqueue(const Queue *q, void *element) {
    /* implementation of the enqueue() method */
     Node *p = createNode(element);
     QData *qd = (QData *)q->self;
     if(qd->head == NULL){
        qd->head = p;
        qd->tail = p;
     }else{
        qd->tail->next = p;
        qd->tail = p;
     }
     qd->count++;
     return 1;
}

static int q_front(const Queue *q, void **element) {
    /* implementation of the front() method */
    QData *qd = (QData *)q->self;
    Node *p = qd->head;
    if(p != NULL){
        *element = p->data;
        return 1;
    }else{
        return 0;
    }
}

static int q_dequeue(const Queue *q, void **element) {
    /* implementation of the dequeue() method */
    int status = 0;
    QData *qd = (QData *)q->self;
    if(qd->head == NULL){
        return status;
    }

    Node *p = qd->head;
    qd->head = qd->head->next;

    if(qd->head == NULL){
        qd->tail = NULL;
    }
    *element = p->data;
    status = 1;
    qd->count--;
    free(p);
    return status;
}

static long q_size(const Queue *q) {
    /* implementation of the size() method */
    QData *qd = (QData *)q->self;
    return qd->count;
}

static int q_isEmpty(const Queue *q) {
    /* implementation of the isEmpty() method */
    QData *qd = (QData *)q->self;
    if (qd->head == NULL){
        return 1;
    }else{
        return 0;
    }
}

static void **q_toArray(const Queue *q, long *len) {
    /* implementation of the toArray() method */
    void **tmp = NULL;
    QData *qd = (QData *)q->self;

    if(qd->count > 0L){
        tmp = (void **)malloc(qd->count * sizeof(Data));
        if(tmp != NULL){
            Node *p = qd->head;
            for(int i = 0; i <= qd->count; i++){
                tmp[i] = p->data;
                p = p->next;
            }
            *len = qd->count;
        }
    }
    return tmp;
}

static const Iterator *q_itCreate(const Queue *q) {
    /* implementation of the itCreate() method */
    QData *qd = (QData *)q->self;
    const Iterator *it = NULL;
    void **tmp = q_toArray(q, NULL);

    if(tmp != NULL) {
        it = Iterator_create(qd->count, tmp);
        if(it == NULL){
            free(tmp);
        }
    }
    return it;
}

static const Queue *q_create(const Queue *q);
/* this is just declaring the signature for the create() method; it's
   implementation is provided below */

static Queue template = {
    NULL, q_create, q_destroy, q_clear, q_enqueue, q_front, q_dequeue, q_size,
    q_isEmpty, q_toArray, q_itCreate
};

static const Queue *q_create(const Queue *q) {
    /* implementation of the create() method */
    QData *qd = (QData *)q->self;
    return Queue_create(qd->freeValue);
}

const Queue *LListQueue(void (*freeValue)(void *e)) {
    /* implementation of the structure-specific constructor */
    Queue *q = (Queue *)malloc(sizeof(Queue));
    if (q != NULL){
        QData *qd = (QData *)malloc(sizeof(QData));
        if(qd != NULL){
            qd->head = NULL;
            qd->tail = NULL;
            qd->freeValue = freeValue;
            qd->count = 0L;
            *q = template;
            q->self = qd;
        }else{
            free(q);
        }
    }
    return q;
}

const Queue *Queue_create(void (*freeValue)(void *e)) {
    /* implementation of the generic constructor */
    Queue *q = (Queue *)malloc(sizeof(Queue));
    if (q != NULL){
        QData *qd = (QData *)malloc(sizeof(QData));
        if(qd != NULL){
            qd->head = NULL;
            qd->tail = NULL;
            qd->count = 0L;
            qd->freeValue = freeValue;
            *q = template;
            q->self = qd;
        }else{
            free(q);
        }
    }
    return q;
}
