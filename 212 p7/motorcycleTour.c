#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ADTs/queue.h>
#include <time.h>

#define UNUSED __attribute__((unused))

typedef struct data{
	int L;
	int D;
	int N;
} Data;


int testStation(const Queue *q){
	Data *d;
	int gas = 0;
	int stations = 0;
	while(gas >= 0){
		stations++;
		q->dequeue(q, (void **)&d);
		gas += 10 * d->L;
		gas -= d->D;
		q->enqueue(q, d);
		if (stations == q->size(q)){
			return 1;
		}
	}
	return 0;
}

int main(UNUSED int argc, UNUSED char *argv[]) {
	const Queue *q = Queue_create(NULL);
	int test;
	char buf[BUFSIZ];
	int insNum = 0;
	fgets(buf, BUFSIZ, stdin);
	insNum = atoi(buf);
	for(int i = 0; i < insNum; i++){
		int L, D;
		fgets(buf, BUFSIZ, stdin);
		char *p = strchr(buf, '\n');
		*p = '\0';
		if(buf != NULL){
			sscanf(buf, "%d %d", &L, &D);
			Data *d = (Data *)malloc(sizeof(Data));
			d->L = L;
			d->D = D;
			d->N = i;
			q->enqueue(q, d);
		}
	}
	
	for(int j = 0; j <= q->size(q); j++){
		test = testStation(q);
		Data *data;
		if (test){
			q->dequeue(q, (void **)&data);
			printf("%d\n", data->N);
			q->enqueue(q, data);
			goto cleanup;
		}
	}
	printf("%s\n", "No solution");
	goto cleanup;
	
	cleanup:
		while(q->isEmpty(q) == 0){
			Data *d;
			q->dequeue(q, (void **)&d);
			free(d);
		}
		
		q->destroy(q);

}